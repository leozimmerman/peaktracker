#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){

	ofSetVerticalSync(true);
	ofBackground(54, 54, 54);
	ofSetFrameRate(60);

    //soundStream.listDevices();
    //soundStream.setDeviceID(0); //bear in mind the device id corresponds to all audio devices, including  input-only and output-only devices.
    int ticksPerBuffer = BUFFER_SIZE/64;
    bufferSize = ofxPd::blockSize()*ticksPerBuffer; //pdBlock-64
    inChan  = 0;
	outChan = 1;
	sampleRate = S_RATE;

    peak_tracker.setup(bufferSize, sampleRate);

    drawBuffer.resize(bufferSize);
	middleBuffer.resize(bufferSize);
	audioBuffer.resize(bufferSize);

	drawBins.resize(peak_tracker.fftW_size);
	middleBins.resize(peak_tracker.fftW_size);
	audioBins.resize(peak_tracker.fftW_size);
	printf("fftW Size = %i\n", peak_tracker.fftW_size);

	soundStream.setup(this, outChan, inChan, sampleRate, bufferSize, ticksPerBuffer);	// 0 output channels - 2 input channels - 44100 samples per second - 256 samples per buffer - 4 num buffers (latency)
    core.setup(outChan, inChan, sampleRate, ticksPerBuffer);

    setGUI();
    gui1->setDrawBack(false);

    filename = "-none-";
    xmlName = "-none-";

   	bufferCounter	= 0;
	drawCounter		= 0;
}

//--------------------------------------------------------------

void testApp::update(){

}

//--------------------------------------------------------------
void testApp::draw(){

	ofSetColor(225);
	ofNoFill();
	ofDrawBitmapString("FILE LOADED: "+ filename , 15, 60);
	//ofDrawBitmapString("Set to: "+ ofToString(filterBank.pitchDev) , 140, 420);
	ofDrawBitmapString("XML file: "+ xmlName , 110, 345);


    //ṕor cestiones de sincro entre Frame Rate y Sample Rate??
    soundMutex.lock();
	drawBuffer = middleBuffer;
	drawBins = middleBins;
	soundMutex.unlock();

	// Osciloscope:
	{
        ofPushStyle();
            ofPushMatrix();
            ofTranslate(300, 15, 0);
            ofSetColor(225);
            ofDrawBitmapString("Time Domain", 4, 18);
            ofSetLineWidth(1);
            ofRect(0, 0, 256, 200);
            ofSetColor(245, 58, 135);
            ofSetLineWidth(3);
                ofBeginShape();
                for (int i = 0; i < drawBuffer.size(); i++){
                    ofVertex(i/(bufferSize/256), 100 - drawBuffer[i]*90.0f);//90f-scale
                }
                ofEndShape(false);
            ofPopMatrix();
        ofPopStyle();
	}

	// draw the right channel:
	{
        ofPushStyle();
		ofPushMatrix();
		ofTranslate(600, 15, 0);
		ofSetColor(225);
		ofDrawBitmapString("Frequency domain", 4, 18);
		ofSetLineWidth(1);
		ofRect(0, 0, 256, 200);
		ofSetColor(245, 58, 135);
		ofSetLineWidth(3);
			ofBeginShape();
			for (int i = 0; i < drawBins.size(); i++){
				ofVertex(i/(drawBins.size() / 256), 200 - drawBins[i]*180.0f);
			}
			ofEndShape(false);
		ofPopMatrix();
        ofPopStyle();
	}

	///Draw ATS
	soundMutex.lock();
	{
        ofPushStyle();
        ofPushMatrix();
        ofTranslate (300,250,0);
        //*
        peak_tracker.draw(650,400);
        //*
        ofPopMatrix();
        ofPopStyle();
	}
	soundMutex.unlock();

	ofSetColor(225);

	string reportString =  "Sampling Rate: "+ ofToString(sampleRate) +"\nBuffer size: "+ ofToString(bufferSize) +"\nbuffers received: "+ofToString(bufferCounter)+"\ndraw routines called: "+ofToString(drawCounter)+"\nticks: " + ofToString(soundStream.getTickCount());
	ofDrawBitmapString(reportString, 32, 575);

	drawCounter++;
}

//--------------------------------------------------------------
void testApp::audioIn(float * input, int bufferSize, int nChannels){


}




void testApp::audioOut(float * output, int bufferSize, int nChannels){
    	core.audioRequested(output, bufferSize, nChannels);

    	memcpy(&audioBuffer[0], output, sizeof(float) * bufferSize);
    	peak_tracker.fft->setSignal(&audioBuffer[0]);
    	float* curFft = peak_tracker.fft->getAmplitude(); ///<-------curFFT es la trasnformada/ Amplitde->Magnitude
        memcpy(&audioBins[0], curFft, sizeof(float) * peak_tracker.fft->getBinSize());

        soundMutex.lock();

        middleBuffer = audioBuffer; //buffer con samples para Osciloscopio
        middleBins = audioBins; //bins de la FFT para el FFTSpectrum

        ///Ats
        peak_tracker.fftR  = peak_tracker.fft->getReal();
        peak_tracker.fftI  = peak_tracker.fft->getImaginary();

        peak_tracker.analyze();

        soundMutex.unlock();

        bufferCounter++;
}

//--------------------------------------------------------------
void testApp::keyPressed  (int key){

     if(gui1->hasKeyboardFocus()) {
        return;
    }
    switch (key){
        case 'q':
			break;
        default:
            break;
    }
}
//--------------------------------------------------------------
float testApp::powFreq(float i) {
	return powf(i, 3);
}

//--------------------------------------------------------------
void testApp::exit(){
    printf("------- CLOSING...\n");
    delete gui1;
    core.exit();
    soundStream.stop();
    soundStream.close();
    peak_tracker.exit();
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}
//--------------------------------------------------------------
void testApp::setGUI(){
    float dim = 16;
    float xInit = OFX_UI_GLOBAL_WIDGET_SPACING;
    float length = 255-xInit;

    gui1 = new ofxUICanvas(10, 10, length+xInit, ofGetHeight());

	gui1->addWidgetDown(new ofxUILabel("Peak Tracker", OFX_UI_FONT_LARGE));
    gui1->addSpacer(length-xInit, 2);

    gui1->addWidgetDown(new ofxUILabel("", OFX_UI_FONT_LARGE));//space
	gui1->addWidgetDown(new ofxUILabel("OPEN FILE", OFX_UI_FONT_MEDIUM));
	//gui1->setWidgetFontSize(OFX_UI_FONT_LARGE);
	gui1->setWidgetFontSize(OFX_UI_FONT_MEDIUM);
	gui1->addTextInput("OPEN FILE", "*.wav", length-xInit);
	gui1->addButton("PLAY", false, dim, dim);
	gui1->addButton("STOP", false, dim, dim);
	gui1->addSpacer(length-xInit, 2);
	//gui1->addRangeSlider("NOTE-RANGE", 21, 108, 21, 108, length-xInit,dim);
	gui1->addSlider("VOLUME", 0.0, 1.5, 1.0, length-xInit, dim);
	//gui1->addSlider("SMOOTHING", 0.0, 1.0, 0.0, length-xInit,dim);
	gui1->addSlider("PEAKS", 0.0, 50.0, 6.0, length-xInit,dim);
	//gui1->addWidgetDown(new ofxUILabel("", OFX_UI_FONT_LARGE));//space

	gui1->addSlider("MAX-FREC", 0.0, sampleRate/2, sampleRate/2, length-xInit,dim);
	gui1->addSlider("TRESHOLD", peak_tracker.minValue, peak_tracker.maxValue, -5.5, length-xInit,dim);
	//gui1->addSlider("PITCH-DEV", -1.0, 1.0, 0.0, length-xInit,dim);

	//gui1->addButton("SET-DEV", false, dim, dim);
	gui1->addWidgetDown(new ofxUILabel("", OFX_UI_FONT_LARGE));//space
	gui1->addSpacer(length-xInit, 2);

	gui1->addWidgetDown(new ofxUILabel("SETTINGS", OFX_UI_FONT_MEDIUM));
	gui1->addTextInput("SETTINGS", "*.xml", length-xInit);
	gui1->addButton("SAVE", false, dim, dim);
	gui1->addButton("LOAD", false, dim, dim);
	gui1->addButton("RESET", false, dim, dim);

    gui1->addWidgetDown(new ofxUILabel("", OFX_UI_FONT_LARGE));//space
    gui1->addSpacer(length-xInit, 2);
	gui1->addToggle( "AUDIO-ON/OFF", true, dim, dim);

	ofAddListener(gui1->newGUIEvent,this,&testApp::guiEvent);

}

//--------------------------------------------------------------

void testApp::guiEvent(ofxUIEventArgs &e){
    string name = e.widget->getName();
	int kind = e.widget->getKind();
	static float  pitchFader = 0.0;

	//cout << "got event from: " << name << endl;
	if(name == "OPEN FILE"){
        ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
        if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_ENTER) {
            cout << "ON ENTER: ";
            filename = textinput->getTextString();
            //            ofUnregisterKeyEvents((testApp*)this);
        }
        else if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_FOCUS){
            cout << "ON FOCUS: ";
        }
        else if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_UNFOCUS) {
            cout << "ON BLUR: ";
//            ofRegisterKeyEvents(this);
        }
    }else if (name == "PLAY"){
       	ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (value) core.openPlayFile(filename);
    }else if (name == "STOP"){
       	ofxUIButton *button = (ofxUIButton *) e.widget;
		bool value = button->getValue();
		if (!value) core.pd.sendBang("stopOF");
//    }else if (name == "NOTE-RANGE"){
//        ofxUIRangeSlider *rSlider = (ofxUIRangeSlider *) e.widget;
       // filterBank.midiMinVar = (int) rSlider->getScaledValueLow();
        //rSlider->setValueLow(filterBank.midiMinVar);
        //filterBank.midiMaxVar = (int) rSlider->getScaledValueHigh();
      //  rSlider->setValueHigh(filterBank.midiMaxVar);
    }else if (name == "VOLUME"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        float value = slider->getScaledValue();
        core.pd.sendFloat("volOF", value);
//    }else if (name == "SMOOTHING"){
//        ofxUISlider *slider = (ofxUISlider *) e.widget;
//        peak_tracker.smoothAmnt = slider->getScaledValue();
    }else if (name == "PEAKS"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        peak_tracker.nPeaks = (int) slider->getScaledValue();
        slider->setValue(peak_tracker.nPeaks);
//    }else if (name == "PITCH-DEV"){
//        ofxUISlider *slider = (ofxUISlider *) e.widget;
//        pitchFader = slider->getScaledValue() ;
//    }else if (name == "SET-DEV"){
//       	ofxUIButton *button = (ofxUIButton *) e.widget;
//		bool value = button->getValue();
//		if (value){
//		   // filterBank.pitchDev = pitchFader;
//            //filterBank.setPitchDev();
//        }
    }else if (name == "MAX-FREC"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        peak_tracker.maxFreq = (int) slider->getScaledValue();
    }else if (name == "TRESHOLD"){
        ofxUISlider *slider = (ofxUISlider *) e.widget;
        //filterBank.treshold = slider->getScaledValue() * filterBank.estimateMax;
        peak_tracker.treshold = powf(10.0, slider->getScaledValue()) ;
    }else if (name == "AUDIO-ON/OFF"){
        ofxUIToggle *toggle = (ofxUIToggle *) e.widget;
		bool value = toggle->getValue();
		if (value) soundStream.start();
		else soundStream.stop();
    }else if (name == "SETTINGS"){
        ofxUITextInput *textinput = (ofxUITextInput *) e.widget;
        if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_ENTER)
        {
            cout << "ON ENTER: ";
            xmlName = textinput->getTextString();
            //            ofUnregisterKeyEvents((testApp*)this);
        }
        else if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_FOCUS){
            cout << "ON FOCUS: ";
        }
        else if(textinput->getInputTriggerType() == OFX_UI_TEXTINPUT_ON_UNFOCUS){
            cout << "ON BLUR: ";
//            ofRegisterKeyEvents(this);
        }
    }else if (name == "RESET"){
         gui1->loadSettings("./settings/default.xml");
         printf("Settings RESETED\n");
    }else if (name == "SAVE"){
        string name = "./settings/"+xmlName;
        gui1->saveSettings(name);
        printf("Settings SAVED\n");
    }else if (name == "LOAD"){
        string name = "./settings/"+xmlName;
        gui1->loadSettings(name);
        printf("Settings LOADED\n");
    }

}
//--------------------------------------------------------------

void testApp::mouseMoved(int x, int y ){

//    float value = ofMap (x, 0, ofGetWidth(), 0, sampleRate/2);
//    core.pd.sendFloat("oscOF", value);
//    printf("%f\n", value);

}


//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){
   // enerMax = 0;
    //core.pd.sendBang("playOF");
   // printf("mousePresed\n");

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}

