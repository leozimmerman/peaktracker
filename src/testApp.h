#ifndef _TEST_APP
#define _TEST_APP


#include "ofMain.h"
#include "AppCore.h"
#include "ofxUI.h"
#include "ofxFft.h"
#include "PeakTracker.h"





#define BANDWITH  1.0
#define BUFFER_SIZE 512
#define S_RATE 44100
//#define LIN2dB(x) (double)(20. * log10(x))



class testApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();
		void exit();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		void audioIn(float * input, int bufferSize, int nChannels);
        void audioOut(float * input, int bufferSize, int nChannels);

        float powFreq(float i);
        void setGUI();
        ofxUICanvas *gui1;

        void guiEvent(ofxUIEventArgs &e);

		ofSoundStream soundStream;
		AppCore core;
		PeakTracker peak_tracker;

		int	sampleRate;
		int bufferSize;
		int inChan;
		int outChan;

        int bufferCounter;
		int drawCounter;

        string filename;
        string xmlName;

        ofMutex soundMutex;
        vector<float> drawBins, middleBins, audioBins;
        vector<float> drawBuffer, middleBuffer, audioBuffer;



};

#endif

