#ifndef PEAKTRACKER_H
#define PEAKTRACKER_H

#include "ofMain.h"
#include "ofxFft.h"
//#define LIN2dB(x) (double)(20. * log10(x))

extern "C" {
    #include "./atsa/atsa.h"
}

#define  MAX_PEAKS 50
#define  MIN_AMP -6
#define  MAX_AMP -1


class PeakTracker
{
    public:

        void setup(int iBufferSize, int iSampleRate);
        void draw(int w, int h);
        void exit();
        void setDefaultAnargs();
        void analyze();
        int freqToMidi(float iFrec);
        string midiToNote(int midi);

        ofxFft* fft;
        ANARGS anargs;
        ATS_FFT fftAts;

        int sflen;
        int peaks_size, tracks_size;
        int frame_n;
        int ana_n;
        float *window;
        float norm, f_tmp, sfdur;
        ATS_PEAK *peaks, *tracks;
        ATS_PEAK cpy_peak;
        ATS_FRAME *ana_frames, *unmatched_peaks;
        int n_partials;
        bool anaState;
    //vector<ATS_FRAME*> ana_array;

        float *fftR, *fftI;

        int CONTROLLER;
        int frameCount;

        int bufferSize, sampleRate;

        int fftW_size;

        int nPeaks;
        float treshold;

        float minValue, maxValue;
        int maxFreq;
//
//        vector<float> smth_energies;
//        float smoothAmnt;


    protected:

    private:




};

#endif // PEAKTRACKER_H
