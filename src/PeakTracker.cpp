#include "PeakTracker.h"


void PeakTracker::setup(int iBufferSize, int iSampleRate){

    bufferSize = iBufferSize;
    sampleRate = iSampleRate;

    fft = ofxFft::create(bufferSize, OF_FFT_WINDOW_HAMMING, OF_FFT_FFTW);
    fftW_size = fft->getBinSize();

    setDefaultAnargs();
    peaks = tracks = NULL;
	ana_frames = unmatched_peaks = NULL;
	tracks_size = 0;
	n_partials = 0;
	anaState = false;
	nPeaks = 6;
	treshold = 0.000003162;
	minValue = MIN_AMP;
	maxValue = MAX_AMP;
//	smoothAmnt = 0.0;
//	smth_energies.assign(MAX_PEAKS, 0.0);
	maxFreq = sampleRate;

//	ana_frames = (ATS_FRAME *)malloc(anargs->frames * sizeof(ATS_FRAME));
    int anaFrame_size = anargs.track_len + 1;
    printf("ANASIZE %i\n", anaFrame_size);
	ana_frames = (ATS_FRAME *)malloc( anaFrame_size * sizeof(ATS_FRAME)); //cuantas frames tiene el archivo??

    //set fftAts
    #ifdef FFTW
    printf("-----FFTW\n");
    #else
    fftAts.fdr = (double *)malloc(fftW_size * sizeof(double));
    fftAts.fdi = (double *)malloc(fftW_size * sizeof(double));
    for(int k=0; k<fftAts.size; k++) fftAts.fdr[k] = fftAts.fdi[k] = 0.0f;
    printf("---------OTRA_FFT-----------\n");
    #endif

    fftAts.size = fftW_size;
    fftAts.rate = anargs.srate;

    cout<<"peakTracker setup finished ----" << endl;
   // cout<<"pT sample rate: "<< sampleRate<< endl;

}

//--------------------------------------------------------------

void PeakTracker::exit(){
     free(window);
  free(tracks);
#ifdef FFTW
  fftw_destroy_plan(plan);
  fftw_free(fft.data);
#else
  free(fftAts.fdr);
  free(fftAts.fdi);
#endif

 free(ana_frames);

}
//--------------------------------------------------------------

void PeakTracker::draw(int w, int h){
   ofPushStyle();
   ofRect(0,0,w,h);
   ofDrawBitmapString("Peak Tracking", 4, 18);
   ofFill();
   ofSetLineWidth(3);


    if(ana_frames[ana_n].n_peaks != NULL){
        for (int i=0; i<ana_frames[ana_n].n_peaks; i++){
            if (ana_frames[ana_n].peaks[i].amp  != 0 && ana_frames[ana_n].peaks[i].amp  > treshold
                    && ana_frames[ana_n].peaks[i].frq > 3 && ana_frames[ana_n].peaks[i].frq < maxFreq){///chequear
                float freq = ofMap(ana_frames[ana_n].peaks[i].frq, 0, maxFreq, 0, w);
                float logAmp = log10(ana_frames[ana_n].peaks[i].amp);
                float amp = ofMap(logAmp, minValue, maxValue, 0, h);
                ofSetColor(110);
                ofLine(freq, h-amp, freq, h);
                ofSetColor(245, 58, 135);
                ofCircle(freq, h-amp, 3);
                ofDrawBitmapString(midiToNote(freqToMidi(ana_frames[ana_n].peaks[i].frq)), freq,h-amp-5);
            }
        }
    }

    float logTresh = log10(treshold);
    float tres = ofMap(logTresh, minValue, maxValue, 0, h);
    ofSetColor(80);
    ofSetLineWidth(1);
    ofLine(0,h-tres, w, h-tres);
    ofDrawBitmapString("Treshold" , w-80, h-tres);
    ofPopStyle();

    ofDrawBitmapString("Max: "+ ofToString(maxFreq/2) + " hz" , w-110, h);


}



//--------------------------------------------------------------

void PeakTracker::setDefaultAnargs(){

    sflen = bufferSize;
    window = NULL;

    anargs.srate = (float) sampleRate;
    anargs.start = 0.0;
    sfdur = (float) sflen / anargs.srate;
    anargs.duration = sfdur - anargs.start;
    anargs.lowest_freq = ATSA_LFREQ;
    anargs.highest_freq = ATSA_HFREQ;
    anargs.freq_dev = ATSA_FREQDEV;
    anargs.win_cycles = ATSA_WCYCLES;
    anargs.win_type = ATSA_WTYPE;
    anargs.hop_size = ATSA_HSIZE;
//    anargs.lowest_mag = ATSA_LMAG;
    anargs.lowest_mag = -100;
    anargs.track_len = ATSA_TRKLEN;
    anargs.first_smp = (int)floor(anargs.start * (float)anargs.srate);
    anargs.total_samps = (int)floor(anargs.duration * (float)anargs.srate);
//    /* fundamental cycles */
    anargs.cycle_smp = (int)floor((double)anargs.win_cycles * (double)anargs.srate / (double)anargs.lowest_freq);
//    /* window size */
    anargs.win_size = (anargs.cycle_smp % 2 == 0) ? anargs.cycle_smp+1 : anargs.cycle_smp;
//    /* calculate hop samples */
    anargs.hop_smp = floor( (float)anargs.win_size * anargs.hop_size );
    /* compute total number of frames */
   /// anargs->frames = compute_frames(anargs);
    anargs.SMR_thres = ATSA_SMRTHRES;
    anargs.min_seg_SMR = ATSA_MSEGSMR;
    anargs.last_peak_cont = ATSA_LPKCONT;
    anargs.SMR_cont = ATSA_SMRCONT;
    anargs.fft_size = bufferSize;

     /* make our window */
    window = make_window(anargs.win_type, anargs.win_size);
    /* get window norm */
    norm = window_norm(window, anargs.win_size);

    anargs.fft_mag = (double)anargs.srate / (double)anargs.fft_size;
    anargs.lowest_bin = floor( anargs.lowest_freq / anargs.fft_mag );
    anargs.highest_bin = floor( anargs.highest_freq / anargs.fft_mag );


}
//--------------------------------------------------------------
void PeakTracker::analyze(){
    if(fftR != NULL && fftI != NULL){

        frame_n = CONTROLLER; ///FrameCount
          if (!anaState){
            ana_n = frame_n;
        }else{
            ana_n = 3;
        }
//        printf("FRAME n°: %i ******************************************************* \n", CONTROLLER);

        for (int k=0; k<fftAts.size; k++){
          fftAts.fdr[k] = (double) fftR[k];
          fftAts.fdi[k] = (double) fftI[k];
        }

       ///peak_detection----
        peaks_size = 0;
        peaks = peak_detection(&fftAts, anargs.lowest_bin, anargs.highest_bin, anargs.lowest_mag, norm, &peaks_size);
        //printf("--PeaksSize: %i \n", peaks_size);

        if (peaks != NULL){
            ///* evaluate peaks SMR (masking curves) */
            evaluate_smr(peaks, peaks_size);
            //printf("---------EvalSmr-END-----------\n");
           if (frame_n >0) {
                ///Update tracks
//               tracks = update_tracks(tracks, &tracks_size, anargs.track_len, frame_n, ana_frames, anargs.last_peak_cont) ;
               tracks = update_tracks(tracks, &tracks_size, anargs.track_len, ana_n, ana_frames, anargs.last_peak_cont) ;
              // printf("tracksUpdated\n");
               if (tracks != NULL ){
//                   // do peak matching
//                  unmatched_peaks = peak_tracking(tracks, &tracks_size, peaks, &peaks_size,  anargs.freq_dev, 2.0 * anargs.SMR_cont, &n_partials);
//                 // printf("peaksTracked\n");
//                  /// kill unmatched peaks from previous frame trayectorias no continuadas
//                    if(unmatched_peaks[0].peaks != NULL) {
//                        for(int k=0; k<unmatched_peaks[0].n_peaks; k++) {
//                            cpy_peak = unmatched_peaks[0].peaks[k];
//                            cpy_peak.amp = cpy_peak.smr = 0.0;///set O
//                            peaks = push_peak(&cpy_peak, peaks, &peaks_size);///trayectorias no continuadas como picos actuales en 0
//                        }
//                        free(unmatched_peaks[0].peaks);///*borra unmatched peaks[0]
//                       // printf("unmatchedKill\n");
//                    }
//                  // printf("unmatchedKill\n");
//                  ///* give birth to peaks from new frame */ picos no asignados
//                    if(unmatched_peaks[1].peaks != NULL) {
//                        for(int k=0; k<unmatched_peaks[1].n_peaks; k++) {
//                            tracks = push_peak(&unmatched_peaks[1].peaks[k], tracks, &tracks_size);///agrega los nuevos peaks al tracks y a ana_frames
//                            unmatched_peaks[1].peaks[k].amp = unmatched_peaks[1].peaks[k].smr = 0.0;
//                            ana_frames[ana_n-1].peaks = push_peak(&unmatched_peaks[1].peaks[k], ana_frames[ana_n-1].peaks, &ana_frames[ana_n-1].n_peaks);
//                        }
//                        free(unmatched_peaks[1].peaks);
//                    }
               }else{
                     /* give number to all peaks */
                    qsort(peaks, peaks_size, sizeof(ATS_PEAK), peak_frq_inc);
                    for(int k=0; k<peaks_size; k++) peaks[k].track = n_partials++;
               }
            }else {
                // give number to all peaks porque es el primer frame
                qsort(peaks, peaks_size, sizeof(ATS_PEAK), peak_frq_inc);
                for(int k=0; k<peaks_size; k++) peaks[k].track = n_partials++;
            }
            ///* attach peaks to ana_frames */
            if (anaState){
                ATS_PEAK* free_peaks;
                free_peaks = ana_frames[0].peaks;
                for (int i=0; i<3; i++){
                    ///Swap
                    ana_frames[i].n_peaks = ana_frames[i+1].n_peaks;
                    ana_frames[i].peaks = ana_frames[i+1].peaks;
                }
                free (free_peaks);
            }
            ana_frames[ana_n].peaks = peaks;
           // ana_frames[ana_n].n_peaks = n_partials;
            ana_frames[ana_n].n_peaks = nPeaks;
            //ana_frames[frame_n].time = (double)(win_samps[frame_n] - anargs->first_smp) / (double)anargs->srate;
            /* free memory */
            free(unmatched_peaks);

        }else {
            ana_frames[ana_n].peaks = NULL;
            ana_frames[ana_n].n_peaks = 0;
            //ana_frames[frame_n].time = (double)(win_samps[frame_n] - anargs.first_smp) / (double)anargs.srate; ///no es necesario
       }

         if (frame_n >= 3){
                anaState = true;
         }

//         for (int i=0; i<ana_frames[ana_n].n_peaks; i++){
//            smth_energies[i] *= smoothAmnt;
//            smth_energies[i] += (1-smoothAmnt) * ana_frames[ana_n].peaks[i].amp;
//         }



    CONTROLLER ++;
}

}

//--------------------------------------------------------------
int PeakTracker::freqToMidi(float iFrec){

    int result = round (12*log2(iFrec/440) + 69);
    return (result);
}
//--------------------------------------------------------------
string PeakTracker::midiToNote(int midi){

    string noteName;
    int mod = midi%12;
    switch (mod){
        case 0: noteName = "C";
                break;
        case 1: noteName = "C#";
                break;
        case 2: noteName = "D";
                break;
        case 3: noteName = "D#";
                break;
        case 4: noteName = "E";
                break;
        case 5: noteName = "F";
                break;
        case 6: noteName = "F#";
                break;
        case 7: noteName = "G";
                break;
        case 8: noteName = "G#";
                break;
        case 9: noteName = "A";
                break;
        case 10: noteName = "Bb";
                break;
        case 11: noteName = "B";
                break;
        default:
                break;

    }
    return (noteName);

}

